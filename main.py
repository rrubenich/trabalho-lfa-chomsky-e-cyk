#!/usr/bin/python
# coding=utf-8

import arquivo.leitura
import arquivo.gravacao
import gramatica.chomsky
import gramatica.simplificacao
import gramatica.cyk

	
leitura 	  = arquivo.leitura.Leitura()
gravacao 	  = arquivo.gravacao.Gravacao()
chomsky 	  = gramatica.chomsky.Chomsky()
cyk 	  	  = gramatica.cyk.CYK()
simplificacao = gramatica.simplificacao.Simplificacao()


# fileToRead = raw_input("Caminho do arquivo")
# fileToWrite = raw_input("Caminho para gravacao")
fileToRead = '../Arquivos/gramaticaCYK.txt'
fileToWrite = '../Arquivos/teste.txt'

# Leitura do arquivo
lines 	  = leitura.leArquivo(fileToRead) 
# Transformação em gramatica
gramatica = leitura.leGram(lines) 

# # Etapa 1 da transformação
# 	# Etapa 1 da simplificação
# gramatica = simplificacao.simbolosInuteis(gramatica)
# gramatica = simplificacao.removeNaoAtingidos(gramatica)

# 	# Etapa 3 da simplificação
# gramatica = simplificacao.producoesAB(gramatica)

# 	# Etapa 2 da simplificação
# gramatica = simplificacao.producoesVazias(gramatica)

# # Etapa 1 da transformação
# gramatica = chomsky.newProductions(gramatica)

# # Etapa 2 da transformação
# gramatica = chomsky.subVarsLeft(gramatica)

# # Etapa 2 da transformação
# gramatica = chomsky.onlyVarsLeft(gramatica)

# # Gravação do resultado no arquivo
# gravacao.gravaArquivo(fileToWrite, gramatica)


teste = cyk.cyk(gramatica, "abaab")

print teste