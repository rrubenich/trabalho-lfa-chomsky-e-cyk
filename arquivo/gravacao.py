#!/usr/bin/python
# coding=utf-8

import string

class Gravacao:

	def gravaArquivo(self, fileToWrite, gramatica):
		file  = open(fileToWrite, 'w');

		file.write("Variáveis:\n")
		for variavel in gramatica[0][:-1]:
			file.write(variavel + ", ")
		file.write(gramatica[0][-1])
		file.write("\n")
		
		file.write("Terminais:\n")
		for terminal in gramatica[1][:-1]:
			file.write(terminal + ", ")
		file.write(gramatica[1][-1])
		file.write("\n")
		
		file.write("Produções:\n")
		for variavel, producoes in gramatica[2].iteritems():
			for producao in producoes:
				file.write(variavel[0] + " -> " + producao + "\n")
		
		file.write("Inicial:\n")
		file.write(gramatica[3])
		file.close()

