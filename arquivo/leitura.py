#!/usr/bin/python
# coding=utf-8

import string

class Leitura:

	def leArquivo(self, fileToRead):
		file  = open(fileToRead, 'r');
		lines = file.readlines()
		file.close()
		return lines

	def leGram(self, lines):
		var   = None
		term  = None
		ini   = None
		prod  = []
		gram  = []
		dictProd = {}

		# Remove a primeira linha do arquivo
		del(lines[0])

		# Lê até terminais (Variaveis)
		while lines[0] != "Terminais:\n":
			var = lines[0].replace('\n','').replace(' ','').split(',')
			del(lines[0])
		del(lines[0])

		# Lê até producoes (Terminais)
		while lines[0] != "Produções:\n":
			term = lines[0].replace('\n','').replace(' ','').split(',')
			del(lines[0])
		del(lines[0])

		# Lê até inicial (Producoes)
		while lines[0] != "Inicial:\n":
			prod.append(lines[0].replace('\n','').replace(' ','').split(','))
			del(lines[0])
		del(lines[0])

		# Lê o simbolo inicial
		ini = lines[0].replace('\n','').replace(' ','')

		# Cria dicionario de producoes
		for keyVar in var:
			dictProd.update({keyVar:[]})

		# Atribui producoes ao dicinário
		for valueProd in prod:
			valueProd = valueProd[0].split('->')
			dictProd[valueProd[0]].append(valueProd[1])

		gram.append(var)
		gram.append(term)
		gram.append(dictProd)
		gram.append(ini)

		return gram

	def criaDict(self, gram):

		dictProd = {}

		# Cria dicionario de producoes
		for keyVar in gram[0]:
			dictProd.update({keyVar:[]})

		# Atribui producoes ao dicinário
		for valueProd in gram[2]:
			valueProd = valueProd[0].split('->')
			dictProd[valueProd[0]].append(valueProd[1])

		return dictProd
