#!/usr/bin/python
# coding=utf-8

class Simplificacao:
	

	def dproducoesVazias(self, gramatica):

		dictAux = {}
		dictProd = gramatica[2]

		for keyVar in gramatica[0]:
			dictAux.update({keyVar:[]})

		for variaveis, producoes in dictProd.iteritems():
			for producao in producoes:
				if producao != "ε":
					dictAux[variaveis[0]].append(producao)
				else:
					if dictProd[gramatica[3]].count(producao) == 1:
						if dictAux[gramatica[3]].count(producao) < 1:
							dictAux[gramatica[3]].append(producao)

		gramatica[2] = dictAux
		return gramatica

	# Etapa 2
	# Remove produções vazias
	def producoesVazias(self, gramatica):

		dictProd = gramatica[2]
		dictAux = dictProd.copy()

		for variaveis, producoes in dictProd.iteritems():
			for producao in producoes:
				if producao == "ε":
					dictAux[variaveis].remove(producao)
					if dictAux[gramatica[3]].count(producao) < 1:
						dictAux[gramatica[3]].append(producao)

		gramatica[2] = dictAux
		return gramatica



	# Etapa 1
	# Verifica se qualquer símbolo da gramatica é atingido a partir do inicial recursivamente
	def atingidoDoInicial(self, atual, variaveis, gramatica):
		dictProd = gramatica[2]

		#Roda todas producoes da variavel atual
		for producoes in dictProd[atual]:
			#Roda todas variaveis possíveis a serem produzidas
			for variavelGram in gramatica[0]:
				#Verifica se uma variavel esta sendo produzida na producao
				if variavelGram in producoes:
					#Se producao gera variavel, veririca se a variavel esta na lista de variaveis
					#se nao estiver adiciona e chama recursivamente o metodo
					if not variavelGram in variaveis:
						variaveis.append(variavelGram)
						self.atingidoDoInicial(variavelGram, variaveis, gramatica)

		return variaveis

	def removeNaoAtingidos(self, gramatica):

		variaveis = []
		variaveis.append(gramatica[3])
		variaveis = self.atingidoDoInicial(gramatica[3], variaveis, gramatica)

		#Roda todas variaveis da gramatica
		for variavel in gramatica[0]:
			# se a variavel nao estar na lista de variaveis atingidas, remove suas producoes e ela
			if not variavel in variaveis:
				gramatica[0].remove(variavel)
				del gramatica[2][variavel]

		return gramatica


	# Etapa 1
	# Verfica se todas variaveis possuem produções
	def simbolosInuteis(self, gramatica):

		dictProd = gramatica[2].copy()

		for variavel, producoes in dictProd.iteritems():
			if not producoes:
				del gramatica[2][variavel]
				gramatica[0].remove(variavel)

		return gramatica

	# Etapa 3
	# Produções da forma A -> B
	def producoesAB(self, gramatica):

		dictProd = gramatica[2]

		for variavel, producoes in dictProd.iteritems():
			for item in gramatica[0]:
				if item in producoes:
					gramatica[2][variavel].remove(item)
					for producaoAdicionar in dictProd[item]:
						dictProd[variavel].append(producaoAdicionar)

		return gramatica
