#!/usr/bin/python
# coding=utf-8

import random
import string

class Chomsky:

	# Etapa 2 do processo de Normalização
	# Transformação do lado direito das produções de comprimento maior ou igual a dois

	# Cria novas produções para todas variaveis
	def newProductions(self, gramatica):

		dictProd  = gramatica[2]
		terminais = gramatica[1]
		dictAux   = {}

		for terminal in terminais:
			novaVar = random.choice(string.ascii_letters).upper()

			while novaVar in dictProd:
				novaVar = random.choice(string.ascii_letters).upper()
				
			dictProd.update({novaVar:[terminal]})
			dictAux.update({terminal:[novaVar]})
			
			gramatica[0].append(novaVar)

		gramatica.append(dictAux)


		return gramatica


	# Transforma o lado direito das producoes substituindo suas variaveis por produções
	def subVarsLeft(self, gramatica):

		dictAux = {}

		#itens que podem ser produzidos (variaveis e terminais)
		itensPossiveis = gramatica[0] + gramatica[1]

		for variaveis, producoes in gramatica[2].iteritems():
			for variavel in variaveis:
				for producao in producoes:

					# conta o número de itens do lado direito (entre variaveis e producoes)
					qtdItens = 0
					for item in itensPossiveis:
						qtdItens += producao.count(item)

					# Se for maior que 1 deve efetuar mudanças
					if qtdItens > 1:
						while qtdItens > 1:
							# não necessário, se não pegará letra por letra
							# for sub in producao:
							for terminal in gramatica[1]:
								if terminal in producao:
									#Se existir uma variavel nesta producão, substitua por uma variavel correspondente
									novoLadoDireito = gramatica[4].get(terminal)
									producao = producao.replace(terminal, novoLadoDireito[0])

							qtdItens -= 1

						# Adiciona ao novo dicionário
						if not variavel[0] in dictAux.keys():
							dictAux.update({variavel[0]:[producao]})
						else:
							dictAux[variavel[0]].append(producao)

					# Se o tamanho for igual a um significa que só gera um terminal, então já está pronto
					else:
						if not variavel[0] in dictAux.keys():
							dictAux.update({variavel[0]:[producao]})
						else:
							dictAux[variavel[0]].append(producao)

		del gramatica[4] #remove o dicionario de producões auxiliar
		gramatica[2] = dictAux

		return gramatica
	


	def onlyVarsLeft(self, gramatica):

		dictAux = {}

		#itens que podem ser produzidos (variaveis e terminais)
		itensPossiveis = gramatica[0]
		
		for variaveis, producoes in gramatica[2].iteritems():
			for variavel in variaveis:
				for producao in producoes:

					# conta o número de itens do lado direito (entre variaveis e producoes)
					qtdItens = 0
					for item in itensPossiveis:
						qtdItens += producao.count(item)

					# Problemas daqui pra frente, só funciona CHAR por CHAR
					# Se for maior que 1 deve efetuar mudanças
					if qtdItens > 2:
						for itemCount in range(0,qtdItens-2):
							
							novaVar = random.choice(string.ascii_letters).upper()
							novaProducao = producao[1:]
							antigaProducao = producao[:1]
									
							while novaVar in gramatica[0] or novaVar in dictAux:
								novaVar = random.choice(string.ascii_letters).upper()

							gramatica[0].append(novaVar)


						if not variavel[0] in dictAux:
							dictAux.update({variavel[0]:[antigaProducao+novaVar]})
						else:
							dictAux[variavel[0]].append(antigaProducao+novaVar)

						if not novaVar in dictAux:
							dictAux.update({novaVar:[novaProducao]})
						else:
							dictAux[novaVar].append(novaProducao)


					# Se o tamanho for igual a um significa que só gera um terminal, então já está pronto
					else:
						if not variavel[0] in dictAux:
							dictAux.update({variavel[0]:[producao]})
						else:
							dictAux[variavel[0]].append(producao)

		gramatica[2] = dictAux

		return gramatica