try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Forma normal de Chomsky',
    'author': 'Rafael Rubenich',
    'author_email': 'rfrubenich@gmail.com',
    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['arquivo'],
    'scripts': [],
    'name': 'chomsky'
}

setup(**config)